﻿using DAL;
using DAL.Repo;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace BazaarSite.Controllers
{
    public class LanguageActionFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContextBase context = filterContext.RequestContext.HttpContext;
            string cookie = context.Request.Cookies["culture"] != null && context.Request.Cookies["culture"].Value != "" ? context.Request.Cookies["culture"].Value : ConfigurationManager.AppSettings["DefaultLanguage"];
            if (cookie != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(cookie);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(cookie);
            }
        }
    }

    public class HomeController : Controller
    {
        private DatabaseConnection db = new DatabaseConnection();

        public void ChangeLanguage(string ShortName)
        {
            Response.Cookies.Remove("culture");
            Response.Cookies.Add(new HttpCookie("culture")
            {
                Name = "culture",
                Expires = DateTime.Now.AddDays(3),
                Path = "/",
                Value = ShortName
            });
            Uri url = System.Web.HttpContext.Current.Request.UrlReferrer;
            Response.Redirect(url.ToString());
        }

        public ActionResult _Header(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            main.AllLanguages = main.SiteGetAllLanguages();
            main.Pages = main.SiteGetPage(friendlyUrl);
            main.AllPages = main.SiteGetAllPages(lang);
            main.AllCampaigns = main.SiteGetAllCampaigns(lang);
            main.AllChannels = main.SiteGetAllChannels(lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(lang);
            main.Preferences = main.SiteGetPreferences();
            ViewBag.Current = friendlyUrl;
            return View(main);
        }

        public ActionResult _Footer()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllPages = main.SiteGetAllPages(lang);
            main.AllSocialMedia = main.SiteGetAllSocialMedia();
            main.MultiPreferences = main.SiteGetMultiPreferences(lang);
            return View(main);
        }

        public ActionResult Index()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            main.AllSlider = main.SiteGetAllSlider(lang);
            main.AllCampaigns = main.SiteGetAllCampaigns(lang);
            main.AllChannels = main.SiteGetAllChannels(lang);
            main.AllServices = main.SiteGetAllServices(lang);
            main.AllDealerships = main.SiteGetAllDealerships(lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(lang);
            return View(main);
        }

        public ActionResult Pages(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Pages = main.SiteGetPage(friendlyUrl);
            return View(main);
        }

        public ActionResult Corporate(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Pages = main.SiteGetPage(friendlyUrl);
            main.AllPages = main.SiteGetAllPages(lang);
            return View(main);
        }

        public ActionResult Services(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            main.Pages = main.SiteGetPage(friendlyUrl);
            main.AllServices = main.SiteGetAllServices(lang);
            main.AllDealerships = main.SiteGetAllDealerships(lang);
            return View(main);
        }

        public ActionResult Campaigns(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllCampaigns = main.SiteGetAllCampaigns(lang);
            return View(main);
        }

        public ActionResult Contact(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            main.Pages = main.SiteGetPage(friendlyUrl);
            return View(main);
        }

        public ActionResult Preferences()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Preferences = main.SiteGetPreferences();
            return View(main);
        }

        [HttpPost]
        public ActionResult Contact(FormCollection fc)
        {
            AdminMainProgress.ContactFormSave(fc["NameSurname"], fc["Email"], fc["Message"], db);
            ViewBag.Result = true;
            Uri url = System.Web.HttpContext.Current.Request.UrlReferrer;
            return Content("<script language='javascript' type='text/javascript'>window.location = '" + url.ToString() + "?r=1';</script>");
        }

        public ActionResult CustomerForm(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            main.Pages = main.SiteGetPage(friendlyUrl);
            return View(main);
        }

        [HttpPost]
        public ActionResult CustomerForm(FormCollection fc)
        {
            AdminMainProgress.CustomerFormSave(fc["CompanyName"], fc["Signboard"], fc["RelatedPerson"], fc["Address"], fc["Phone"], fc["Fax"], fc["Email"], fc["Tax"], fc["TaxNo"], db);
            Uri url = System.Web.HttpContext.Current.Request.UrlReferrer;
            return Content("<script language='javascript' type='text/javascript'>window.location = '" + url.ToString() + "?r=1';</script>");
        }

        public ActionResult NotFound()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            return View(main);
        }
    }
}