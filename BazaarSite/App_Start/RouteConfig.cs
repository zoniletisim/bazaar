﻿using DAL;
using DAL.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BazaarSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            DatabaseConnection db = new DatabaseConnection();
            var data = db.Pages.Where(x => x.Active == true && x.Deleted == false).ToList();

            foreach (var item in data)
            {
                routes.MapRoute(
                name: item.FriendlyUrl.ToLower() + item.Id,
                url: item.FriendlyUrl.ToLower(),
                defaults: new { controller = "Home", action = (item.View.ToString() != string.Empty ? item.View.ToLower() : "Pages"), friendlyUrl = item.FriendlyUrl.ToLower() }
                );
            }

            routes.MapRoute(
            name: "ChangeLanguage",
            url: "ChangeLanguage/{shortName}",
            defaults: new { controller = "Home", action = "ChangeLanguage", shortName = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "NotFound",
            url: "NotFound",
            defaults: new { controller = "Home", action = "NotFound" }
            );

            routes.MapRoute(
            name: "Default",
            url: "{controller}/{action}/{friendlyUrl}",
            defaults: new { controller = "Home", action = "Index", friendlyUrl = UrlParameter.Optional }
            );
        }
    }
}