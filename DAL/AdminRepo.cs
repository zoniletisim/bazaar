﻿using DAL.Repo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
namespace DAL
{
    public class AdminMainProgress
    {
        private int _login;

        public List<PanelMenu> AllPanelMenu { get; set; }
        public List<PanelUsers> AllPanelUsers { get; set; }
        public Preferences Preferences { get; set; }
        public List<SocialMedia> AllSocialMedia { get; set; }
        public List<Languages> AllLanguages { get; set; }
        public List<MultiPreferences> AllMultiPreferences { get; set; }
        public MultiPreferences MultiPreferences { get; set; }
        public List<Slider> AllSlider { get; set; }
        public List<Pages> AllPages { get; set; }
        public Pages Pages { get; set; }
        public List<Campaigns> AllCampaigns { get; set; }
        public Campaigns Campaigns { get; set; }
        public List<CampaignCategories> AllCampaignCategories { get; set; }
        public CampaignCategories CampaignCategories { get; set; }
        public List<Translates> AllTranslates { get; set; }
        public Translates Translates { get; set; }
        public List<Channels> AllChannels { get; set; }
        public Channels Channels { get; set; }
        public List<Services> AllServices { get; set; }
        public Services Services { get; set; }
        public List<Dealerships> AllDealerships { get; set; }
        public Dealerships Dealerships { get; set; }
        public List<ContactForms> AllContactForms { get; set; }
        public List<CustomerForm> AllCustomerForm { get; set; }

        DatabaseConnection db = new DatabaseConnection();

        public AdminMainProgress(object login)
        {
            this._login = login.ObjectConvertToInt();
        }

        public static PanelUsers AdminLogin(string email, string password, DatabaseConnection db)
        {
            string hashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");
            var user = db.PanelUsers.Where(x => x.Email == email && x.Password == hashedPassword && x.Active == true && x.Deleted == false).FirstOrDefault();
            return user;
        }

        //PanelUser Start
        public List<PanelUsers> GetPanelUsers()
        {
            var list = db.PanelUsers.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PanelUsersSave(string Email, string Password, string Name, string Surname, int Role, int Owner, string Active, object login, DatabaseConnection db)
        {
            var isHave = db.PanelUsers.Where(x => x.Email == Email && x.Deleted == false).FirstOrDefault();

            if (isHave == null)
            {
                string hashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(Password, "SHA1");
                var DataActive = false;
                if (Active == "0")
                    DataActive = true;

                var paneluser = new PanelUsers();
                paneluser.Email = Email;
                paneluser.Password = hashedPassword;
                paneluser.Name = Name;
                paneluser.Surname = Surname;
                paneluser.Role = Role;
                paneluser.Owner = Owner;
                paneluser.CreatedDate = DateTime.Now;
                paneluser.Active = DataActive;
                paneluser.Deleted = false;
                db.PanelUsers.Add(paneluser);
                db.SaveChanges();
            }
        }

        public static void PanelUsersUpdate(string Id, string Email, string Name, string Surname, int Role, int Owner, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var isHave = db.PanelUsers.Where(x => x.Email == Email && x.Deleted == false && x.Id != ConvertId).FirstOrDefault();

            if (isHave == null)
            {
                var DataActive = false;
                if (Active == "0")
                    DataActive = true;

                var paneluser = db.PanelUsers.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
                if (paneluser != null)
                {
                    paneluser.Email = Email;
                    paneluser.Name = Name;
                    paneluser.Surname = Surname;
                    paneluser.Role = Role;
                    paneluser.Owner = Owner;
                    paneluser.UpdatedDate = DateTime.Now;
                    paneluser.Active = DataActive;
                    db.SaveChanges();
                }
            }
        }

        public static string PanelUsersDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.PanelUsers.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PanelUsersIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var paneluserUpdate = db.PanelUsers.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            paneluserUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //PanelUser End

        //PanelMenu Start
        public List<PanelMenu> GetPanelMenu()
        {
            var list = db.PanelMenu.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PanelMenuSave(int ParentId, string Name, string Url, string Redirect, int Access, string Active, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var panelmenu = new PanelMenu();
            panelmenu.ParentId = ParentId;
            panelmenu.Name = Name;
            panelmenu.Url = Url;
            panelmenu.Redirect = Redirect;
            panelmenu.Icon = "dash";
            panelmenu.Access = Access;
            panelmenu.Order = db.PanelMenu.Count() + 1;
            panelmenu.Active = DataActive;
            panelmenu.Deleted = false;
            db.PanelMenu.Add(panelmenu);
            db.SaveChanges();
        }

        public static void PanelMenuUpdate(string Id, int ParentId, string Name, string Url, string Redirect, int Access, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var panelmenu = db.PanelMenu.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (panelmenu != null)
            {
                panelmenu.ParentId = ParentId;
                panelmenu.Name = Name;
                panelmenu.Url = Url;
                panelmenu.Redirect = Redirect;
                panelmenu.Icon = "dash";
                panelmenu.Access = Access;
                panelmenu.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string PanelMenuDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.PanelMenu.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PanelMenuIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var panelmenuUpdate = db.PanelMenu.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            panelmenuUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PanelMenuSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.PanelMenu.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //PanelMenu End

        //Preferences Start
        public List<Preferences> GetPreferences()
        {
            var list = db.Preferences.Where(x => x.Id == 1).ToList();
            return list;
        }

        public static void PreferencesUpdate(string Logo, string Favicon, string GoogleAnalytics, int Owner, object login, DatabaseConnection db)
        {
            var preferences = db.Preferences.Where(x => x.Id == 1).FirstOrDefault();
            if (preferences != null)
            {
                preferences.Logo = Logo;
                preferences.Favicon = Favicon;
                preferences.GoogleAnalytics = GoogleAnalytics;
                preferences.UpdatedDate = DateTime.Now;
                preferences.Owner = Owner;
                db.SaveChanges();
            }
        }
        //Preferences End

        //SocialMedia Start
        public List<SocialMedia> GetSocialMedia()
        {
            var list = db.SocialMedia.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void SocialMediaSave(string Name, string Link, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var socialmedia = new SocialMedia();
            socialmedia.Name = Name;
            socialmedia.Link = Link;
            socialmedia.Order = db.SocialMedia.Count() + 1;
            socialmedia.CreatedDate = DateTime.Now;
            socialmedia.Owner = Owner;
            socialmedia.Active = DataActive;
            socialmedia.Deleted = false;
            db.SocialMedia.Add(socialmedia);
            db.SaveChanges();
        }

        public static void SocialMediaUpdate(string Id, string Name, string Link, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var socialmedia = db.SocialMedia.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (socialmedia != null)
            {
                socialmedia.Name = Name;
                socialmedia.Link = Link;
                socialmedia.UpdatedDate = DateTime.Now;
                socialmedia.Owner = Owner;
                socialmedia.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string SocialMediaDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.SocialMedia.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool SocialMediaIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var socialmediaUpdate = db.SocialMedia.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            socialmediaUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool SocialMediaSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.SocialMedia.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //SocialMedia End

        //Languages Start
        public List<Languages> GetLanguages()
        {
            var list = db.Languages.Where(x => x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public static void LanguagesSave(string Name, string ShortName, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var languages = new Languages();
            languages.Name = Name;
            languages.ShortName = ShortName;
            languages.Order = db.Languages.Count() + 1;
            languages.CreatedDate = DateTime.Now;
            languages.Owner = Owner;
            languages.Active = DataActive;
            languages.Deleted = false;
            db.Languages.Add(languages);
            db.SaveChanges();
        }

        public static void LanguagesUpdate(string Id, string Name, string ShortName, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var languages = db.Languages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (languages != null)
            {
                languages.Name = Name;
                languages.ShortName = ShortName;
                languages.UpdatedDate = DateTime.Now;
                languages.Owner = Owner;
                languages.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string LanguagesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Languages.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool LanguagesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var languagesUpdate = db.Languages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            languagesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool LanguagesSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Languages.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Languages End

        public object SelectLanguage(object LanguageId)
        {
            if (LanguageId == null)
            {
                LanguageId = 1;
            }

            return LanguageId;
        }

        //MultiPreferences Start
        public List<MultiPreferences> GetMultiPreferences()
        {
            var list = db.MultiPreferences.ToList();
            return list;
        }

        public static void MultiPreferencesUpdate(int LanguageId, string Title, string Description, string Keywords, string Catalog, string Footer, string Address, string SupportPhone, string Phone, string Fax, string Email, int Owner, object login, DatabaseConnection db)
        {
            var multipreferences = db.MultiPreferences.Where(x => x.LanguageId == LanguageId).FirstOrDefault();
            if (multipreferences != null)
            {
                multipreferences.LanguageId = LanguageId;
                multipreferences.Title = Title;
                multipreferences.Description = Description;
                multipreferences.Keywords = Keywords;
                multipreferences.Catalog = Catalog;
                multipreferences.Footer = Footer;
                multipreferences.Address = Address;
                multipreferences.SupportPhone = SupportPhone;
                multipreferences.Phone = Phone;
                multipreferences.Fax = Fax;
                multipreferences.Email = Email;
                multipreferences.UpdatedDate = DateTime.Now;
                multipreferences.Owner = Owner;
                db.SaveChanges();
            }
            else
            {
                var multipreferences2 = new MultiPreferences();
                multipreferences2.LanguageId = LanguageId;
                multipreferences2.Title = Title;
                multipreferences2.Description = Description;
                multipreferences2.Keywords = Keywords;
                multipreferences2.Catalog = Catalog;
                multipreferences2.Footer = Footer;
                multipreferences2.Address = Address;
                multipreferences2.SupportPhone = SupportPhone;
                multipreferences2.Phone = Phone;
                multipreferences2.Fax = Fax;
                multipreferences2.Email = Email;
                multipreferences2.UpdatedDate = DateTime.Now;
                multipreferences2.Owner = Owner;
                db.MultiPreferences.Add(multipreferences2);
                db.SaveChanges();
            }
        }
        //MultiPreferences End

        //Pages Start
        public List<Pages> GetPages()
        {
            var list = db.Pages.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PagesSave(int DataId, int LanguageId, int ParentId, string Title, string ShortContent, string Content, string Banner, string Thumbnail, string Image, string Video, string File, string View, string ShowMenu, string ShowFooter, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var DataShowMenu = false;
            if (ShowMenu == "0")
                DataShowMenu = true;

            var DataShowFooter = false;
            if (ShowFooter == "0")
                DataShowFooter = true;

            var pages = new Pages();
            if (DataId != 0) pages.DataId = DataId;
            pages.LanguageId = LanguageId;
            pages.ParentId = ParentId;
            pages.Title = Title;
            pages.ShortContent = ShortContent;
            pages.Content = Content;
            pages.Banner = Banner;
            pages.Thumbnail = Thumbnail;
            pages.Image = Image;
            pages.Video = Video;
            pages.File = File;
            pages.View = View;
            pages.ShowMenu = DataShowMenu;
            pages.ShowFooter = DataShowFooter;
            pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            pages.Keywords = Keywords;
            pages.Description = Description;
            pages.Order = db.Pages.Count() + 1;
            pages.CreatedDate = DateTime.Now;
            pages.Owner = Owner;
            pages.Active = DataActive;
            pages.Deleted = false;

            //Friendly Url
            var ParentData = db.Pages.Where(x => x.DataId == ParentId && x.LanguageId == LanguageId).FirstOrDefault();

            if (FriendlyUrl == string.Empty)
            {
                if (ParentData != null)
                {
                    FriendlyUrl = ParentData.Title + " " + Title;
                }
                else
                {
                    FriendlyUrl = Title;
                }
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.Pages.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.Pages.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.Pages.Add(pages);
            db.SaveChanges();

            if (DataId == 0)
            {
                var pages2 = db.Pages.Where(x => x.Id == pages.Id).FirstOrDefault();
                pages2.DataId = pages.Id;
                db.SaveChanges();
            }
        }

        public static void PagesUpdate(string Id, int DataId, int LanguageId, int ParentId, string Title, string ShortContent, string Content, string Banner, string Thumbnail, string Image, string Video, string File, string View, string ShowMenu, string ShowFooter, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;
            var DataShowMenu = false;
            if (ShowMenu == "0")
                DataShowMenu = true;
            var DataShowFooter = false;
            if (ShowFooter == "0")
                DataShowFooter = true;

            var pages = db.Pages.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (pages != null)
            {
                pages.DataId = DataId;
                pages.ParentId = ParentId;
                pages.Title = Title;
                pages.ShortContent = ShortContent;
                pages.Content = Content;
                pages.Banner = Banner;
                pages.Thumbnail = Thumbnail;
                pages.Image = Image;
                pages.Video = Video;
                pages.File = File;
                pages.View = View;
                pages.ShowMenu = DataShowMenu;
                pages.ShowFooter = DataShowFooter;
                pages.Keywords = Keywords;
                pages.Description = Description;
                pages.UpdatedDate = DateTime.Now;
                pages.Owner = Owner;
                pages.Active = DataActive;

                //Friendly Url
                var ParentData = db.Pages.Where(x => x.DataId == ParentId && x.LanguageId == LanguageId).FirstOrDefault();

                if (FriendlyUrl == string.Empty)
                {
                    if (ParentData != null)
                    {
                        FriendlyUrl = ParentData.Title + " " + Title;
                    }
                    else
                    {
                        FriendlyUrl = Title;
                    }
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.Pages.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.Pages.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string PagesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Pages.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PagesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var pagesUpdate = db.Pages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            pagesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PagesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Pages.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Pages End

        //Translates Start
        public List<Translates> GetTranslates()
        {
            var list = db.Translates.ToList();
            return list;
        }

        public static void TranslatesSave(int DataId, int LanguageId, string Description, string Text, int Owner, object login, DatabaseConnection db)
        {
            var translates = new Translates();
            if (DataId != 0) translates.DataId = DataId;
            translates.LanguageId = LanguageId;
            translates.Description = Description;
            translates.Text = Text;
            translates.CreatedDate = DateTime.Now;
            translates.Owner = Owner;
            db.Translates.Add(translates);
            db.SaveChanges();

            if (DataId == 0)
            {
                var translates2 = db.Translates.Where(x => x.Id == translates.Id).FirstOrDefault();
                translates2.DataId = translates.Id;
                db.SaveChanges();
            }
        }

        public static void TranslatesUpdate(string Id, int DataId, int LanguageId, string Description, string Text, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();

            var translates = db.Translates.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId).FirstOrDefault();
            if (translates != null)
            {
                translates.DataId = DataId;
                translates.Description = Description;
                translates.Text = Text;
                translates.UpdatedDate = DateTime.Now;
                translates.Owner = Owner;
                db.SaveChanges();
            }
        }
        //Translates End

        //CampaignCategories Start
        public List<CampaignCategories> GetCampaignCategories()
        {
            var list = db.CampaignCategories.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void CampaignCategoriesSave(int DataId, int LanguageId, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var campaigncategories = new CampaignCategories();
            if (DataId != 0) campaigncategories.DataId = DataId;
            campaigncategories.LanguageId = LanguageId;
            campaigncategories.Title = Title;
            campaigncategories.Order = db.CampaignCategories.Count() + 1;
            campaigncategories.CreatedDate = DateTime.Now;
            campaigncategories.Owner = Owner;
            campaigncategories.Active = DataActive;
            campaigncategories.Deleted = false;
            db.CampaignCategories.Add(campaigncategories);
            db.SaveChanges();

            if (DataId == 0)
            {
                var campaigncategories2 = db.CampaignCategories.Where(x => x.Id == campaigncategories.Id).FirstOrDefault();
                campaigncategories2.DataId = campaigncategories.Id;
                db.SaveChanges();
            }
        }

        public static void CampaignCategoriesUpdate(string Id, int DataId, int LanguageId, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var campaigncategories = db.CampaignCategories.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (campaigncategories != null)
            {
                campaigncategories.DataId = DataId;
                campaigncategories.Title = Title;
                campaigncategories.UpdatedDate = DateTime.Now;
                campaigncategories.Owner = Owner;
                campaigncategories.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string CampaignCategoriesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var data = db.CampaignCategories.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (data != null)
                        data.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool CampaignCategoriesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var campaigncategoriesUpdate = db.CampaignCategories.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            campaigncategoriesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool CampaignCategoriesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.CampaignCategories.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //CampaignCategories End

        //Campaigns Start
        public List<Campaigns> GetCampaigns()
        {
            var list = db.Campaigns.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void CampaignsSave(int DataId, int LanguageId, int ParentId, string Title, string ShortContent, string Content, string OldPrice, string NewPrice, string InfoText, string InfoTextColor, string Image, DateTime StartDate, DateTime EndDate, string Hashtags, string Favorite, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var DataFavorite = false;
            if (Favorite == "0")
                DataFavorite = true;

            if (Favorite == "0")
            {
                var list2 = db.Campaigns.ToList();
                foreach (var item in list2)
                {
                    item.Favorite = false;
                    db.SaveChanges();
                }
            }

            var campaigns = new Campaigns();
            if (DataId != 0) campaigns.DataId = DataId;
            campaigns.LanguageId = LanguageId;
            campaigns.ParentId = ParentId;
            campaigns.Title = Title;
            campaigns.ShortContent = ShortContent;
            campaigns.Content = Content;
            campaigns.OldPrice = OldPrice;
            campaigns.NewPrice = NewPrice;
            campaigns.InfoText = InfoText;
            campaigns.InfoTextColor = InfoTextColor;
            campaigns.Image = Image;
            campaigns.StartDate = StartDate;
            campaigns.EndDate = EndDate;
            campaigns.Hashtags = Hashtags;
            campaigns.Favorite = DataFavorite;
            campaigns.CreatedDate = DateTime.Now;
            campaigns.Owner = Owner;
            campaigns.Active = DataActive;
            campaigns.Deleted = false;
            db.Campaigns.Add(campaigns);
            db.SaveChanges();

            if (DataId == 0)
            {
                var campaigns2 = db.Campaigns.Where(x => x.Id == campaigns.Id).FirstOrDefault();
                campaigns2.DataId = campaigns.Id;
                db.SaveChanges();
            }
        }

        public static void CampaignsUpdate(string Id, int DataId, int LanguageId, int ParentId, string Title, string ShortContent, string Content, string OldPrice, string NewPrice, string InfoText, string InfoTextColor, string Image, DateTime StartDate, DateTime EndDate, string Hashtags, string Favorite, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var DataFavorite = false;
            if (Favorite == "0")
                DataFavorite = true;

            if (Favorite == "0")
            {
                var list2 = db.Campaigns.ToList();
                foreach (var item in list2)
                {
                    item.Favorite = false;
                    db.SaveChanges();
                }
            }

            var campaigns = db.Campaigns.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (campaigns != null)
            {
                campaigns.DataId = DataId;
                campaigns.ParentId = ParentId;
                campaigns.Title = Title;
                campaigns.ShortContent = ShortContent;
                campaigns.Content = Content;
                campaigns.OldPrice = OldPrice;
                campaigns.NewPrice = NewPrice;
                campaigns.InfoText = InfoText;
                campaigns.InfoTextColor = InfoTextColor;
                campaigns.Image = Image;
                campaigns.StartDate = StartDate;
                campaigns.EndDate = EndDate;
                campaigns.Hashtags = Hashtags;
                campaigns.Favorite = DataFavorite;
                campaigns.UpdatedDate = DateTime.Now;
                campaigns.Owner = Owner;
                campaigns.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string CampaignsDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Campaigns.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool CampaignsIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var campaignsUpdate = db.Campaigns.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            campaignsUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //Campaigns End

        //Slider Start
        public List<Slider> GetSlider()
        {
            var list = db.Slider.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void SliderSave(int LanguageId, int ParentId, string Title, string Text, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var slider = new Slider();
            slider.LanguageId = LanguageId;
            slider.ParentId = ParentId;
            slider.Title = Title;
            slider.Text = Text;
            slider.Image = Image;
            slider.Order = db.Slider.Count() + 1;
            slider.CreatedDate = DateTime.Now;
            slider.Owner = Owner;
            slider.Active = DataActive;
            slider.Deleted = false;
            db.Slider.Add(slider);
            db.SaveChanges();
        }

        public static void SliderUpdate(string Id, int LanguageId, int ParentId, string Title, string Text, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var slider = db.Slider.Where(x => x.Id == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (slider != null)
            {
                slider.ParentId = ParentId;
                slider.Title = Title;
                slider.Text = Text;
                slider.Image = Image;
                slider.UpdatedDate = DateTime.Now;
                slider.Owner = Owner;
                slider.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string SliderDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Slider.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool SliderIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var sliderUpdate = db.Slider.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            sliderUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool SliderSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Slider.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Slider End

        //Channels Start
        public List<Channels> GetChannels()
        {
            var list = db.Channels.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void ChannelsSave(int DataId, int LanguageId, string Title, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var channels = new Channels();
            if (DataId != 0) channels.DataId = DataId;
            channels.LanguageId = LanguageId;
            channels.Title = Title;
            channels.Image = Image;
            channels.Order = db.Channels.Count() + 1;
            channels.CreatedDate = DateTime.Now;
            channels.Owner = Owner;
            channels.Active = DataActive;
            channels.Deleted = false;
            db.Channels.Add(channels);
            db.SaveChanges();

            if (DataId == 0)
            {
                var channels2 = db.Channels.Where(x => x.Id == channels.Id).FirstOrDefault();
                channels2.DataId = channels.Id;
                db.SaveChanges();
            }
        }

        public static void ChannelsUpdate(string Id, int DataId, int LanguageId, string Title, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var channels = db.Channels.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (channels != null)
            {
                channels.DataId = DataId;
                channels.Title = Title;
                channels.Image = Image;
                channels.UpdatedDate = DateTime.Now;
                channels.Owner = Owner;
                channels.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string ChannelsDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Channels.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool ChannelsIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var channelsUpdate = db.Channels.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            channelsUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool ChannelsSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Channels.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Channels End

        //Services Start
        public List<Services> GetServices()
        {
            var list = db.Services.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void ServicesSave(int DataId, int LanguageId, string Title, string Content, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var services = new Services();
            if (DataId != 0) services.DataId = DataId;
            services.LanguageId = LanguageId;
            services.Title = Title;
            services.Content = Content;
            services.Image = Image;
            services.CreatedDate = DateTime.Now;
            services.Owner = Owner;
            services.Active = DataActive;
            services.Deleted = false;
            db.Services.Add(services);
            db.SaveChanges();

            if (DataId == 0)
            {
                var services2 = db.Services.Where(x => x.Id == services.Id).FirstOrDefault();
                services2.DataId = services.Id;
                db.SaveChanges();
            }
        }

        public static void ServicesUpdate(string Id, int DataId, int LanguageId, string Title, string Content, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var services = db.Services.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (services != null)
            {
                services.DataId = DataId;
                services.Title = Title;
                services.Content = Content;
                services.Content = Content;
                services.Image = Image;
                services.UpdatedDate = DateTime.Now;
                services.Owner = Owner;
                services.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string ServicesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Services.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool ServicesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var servicesUpdate = db.Services.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            servicesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //Services End

        //Dealerships Start
        public List<Dealerships> GetDealerships()
        {
            var list = db.Dealerships.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void DealershipsSave(int DataId, int LanguageId, string Title, string Image, string Link, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var dealerships = new Dealerships();
            if (DataId != 0) dealerships.DataId = DataId;
            dealerships.LanguageId = LanguageId;
            dealerships.Title = Title;
            dealerships.Image = Image;
            dealerships.Link = Link;
            dealerships.CreatedDate = DateTime.Now;
            dealerships.Owner = Owner;
            dealerships.Active = DataActive;
            dealerships.Deleted = false;
            db.Dealerships.Add(dealerships);
            db.SaveChanges();

            if (DataId == 0)
            {
                var dealerships2 = db.Dealerships.Where(x => x.Id == dealerships.Id).FirstOrDefault();
                dealerships2.DataId = dealerships.Id;
                db.SaveChanges();
            }
        }

        public static void DealershipsUpdate(string Id, int DataId, int LanguageId, string Title, string Image, string Link, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var dealerships = db.Dealerships.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (dealerships != null)
            {
                dealerships.DataId = DataId;
                dealerships.Title = Title;
                dealerships.Image = Image;
                dealerships.Link = Link;
                dealerships.UpdatedDate = DateTime.Now;
                dealerships.Owner = Owner;
                dealerships.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string DealershipsDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Dealerships.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool DealershipsIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var dealershipsUpdate = db.Dealerships.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            dealershipsUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //Dealerships End

        //ContactForms Start
        public List<ContactForms> GetContactForms()
        {
            var list = db.ContactForms.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static string ContactFormsDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.ContactForms.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }
        //ContactForms End

        //CustomerForms Start
        public List<CustomerForm> GetCustomerForms()
        {
            var list = db.CustomerForm.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static string CustomerFormsDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.CustomerForm.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }
        //CustomerForms End

        /////////////// <- Panel | Site -> ///////////////
        public List<Languages> SiteGetAllLanguages()
        {
            var list = db.Languages.Where(x => x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public Languages SiteGetLanguage(string ShortName)
        {
            var single = db.Languages.Where(x => x.ShortName == ShortName).FirstOrDefault();
            return single;
        }

        public List<Translates> SiteGetAllTranslates(int SiteLanguage)
        {
            var list = db.Translates.Where(x => x.LanguageId == SiteLanguage).ToList();
            return list;
        }

        public List<Pages> SiteGetAllPages(int SiteLanguage)
        {
            var list = db.Pages.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public Pages SiteGetPage(string friendlyUrl)
        {
            var single = db.Pages.Where(x => x.FriendlyUrl == friendlyUrl && x.Active == true && x.Deleted == false).FirstOrDefault();
            return single;
        }

        public List<Campaigns> SiteGetAllCampaigns(int SiteLanguage)
        {
            var list = db.Campaigns.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Channels> SiteGetAllChannels(int SiteLanguage)
        {
            var list = db.Channels.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<SocialMedia> SiteGetAllSocialMedia()
        {
            var list = db.SocialMedia.Where(x => x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Services> SiteGetAllServices(int SiteLanguage)
        {
            var list = db.Services.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Dealerships> SiteGetAllDealerships(int SiteLanguage)
        {
            var list = db.Dealerships.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Slider> SiteGetAllSlider(int SiteLanguage)
        {
            var list = db.Slider.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public MultiPreferences SiteGetMultiPreferences(int SiteLanguage)
        {
            var single = db.MultiPreferences.Where(x => x.LanguageId == SiteLanguage).FirstOrDefault();
            return single;
        }

        public Preferences SiteGetPreferences()
        {
            var single = db.Preferences.Where(x => x.Id == 1).FirstOrDefault();
            return single;
        }

        public static void ContactFormSave(string NameSurname, string Email, string Message, DatabaseConnection db)
        {
            var contactform = new ContactForms();
            contactform.NameSurname = NameSurname;
            contactform.Email = Email;
            contactform.Message = Message;
            contactform.CreatedDate = DateTime.Now;
            contactform.Deleted = false;
            db.ContactForms.Add(contactform);
            db.SaveChanges();
        }

        public static void CustomerFormSave(string CompanyName, string Signboard, string RelatedPerson, string Address, string Phone, string Fax, string Email, string Tax, string TaxNo, DatabaseConnection db)
        {
            var customerform = new CustomerForm();
            customerform.CompanyName = CompanyName;
            customerform.Signboard = Signboard;
            customerform.RelatedPerson = RelatedPerson;
            customerform.Address = Address;
            customerform.Phone = Phone;
            customerform.Fax = Fax;
            customerform.Email = Email;
            customerform.Tax = Tax;
            customerform.TaxNo = TaxNo;
            customerform.CreatedDate = DateTime.Now;
            customerform.Deleted = false;
            db.CustomerForm.Add(customerform);
            db.SaveChanges();
        }
    }
}