﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using consulta.Models;
using System.Data.Entity.Core.Objects;
namespace consulta.Models
{


    public class CategoryMainProgress
    {
        public List<ConsultaAndVergi_Category> Category { get; set; }
        public List<getPageList_Result> Page { get; set; }


        public static List<ConsultaAndVergi_Category> GetCategoryList()
        {
            using(consultaEntities2 db = new consultaEntities2())
            {

                List<ConsultaAndVergi_Category> categorys = db.ConsultaAndVergi_Category.Include("ConsultaAndVergi_Category1").Where(x => x.parentId == null).ToList();
                return categorys.ToList();

            }
        }
        public static List<getPageList_Result> GetPageList(string categoriUrl)
        {
            using(consultaEntities2 db = new consultaEntities2())
            {

                ObjectResult<getPageList_Result> page = db.getPageList(categoriUrl,1,false);
                return page.ToList();

            }
        }
      
    }
}