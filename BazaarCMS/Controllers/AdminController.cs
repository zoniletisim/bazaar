﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Data;
using DAL;
using DAL.Repo;
using System.Data.SqlClient;
using System.Web.Caching;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections;

namespace BazaarCMS.Controllers
{
    public class AdminController : Controller
    {
        private DatabaseConnection db = new DatabaseConnection();

        //
        // GET: /Admin/

        public void CreateSession()
        {
            if (Session["AdminLanguage"] == null)
            {
                Session["AdminLanguage"] = 1;
            }
        }

        [ChildActionOnly]
        public ActionResult Header()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult LeftMenu()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPanelMenu = main.GetPanelMenu();
            return PartialView(main);
        }

        public ActionResult Index()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPages = main.GetPages();
            return View(main);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            var result = AdminMainProgress.AdminLogin(email, password, db);

            if (result != null)
            {
                Session.Add("LoginId", result.Id);
                Session.Add("Name", result.Name);
                Session.Add("Surname", result.Surname);
                Session.Add("Login", result.Role);
                Session.Add("AdminLanguage", 1);
                return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            Session["LoginId"] = null;
            Session["Name"] = null;
            Session["Surname"] = null;
            Session["Login"] = null;
            return Json(true);
        }

        [ChildActionOnly]
        public ActionResult ChangeLanguage()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllLanguages = main.GetLanguages();
            return PartialView(main);
        }

        [HttpPost]
        public ActionResult AdminLanguage(string language)
        {
            Session["AdminLanguage"] = language;
            return Json(language);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //PanelUsers Start
        public ActionResult PanelUsers()
        {
            CreateSession();
            var result = AdminMainProgress.PanelUsersDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("PanelUsers");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPanelUsers = main.GetPanelUsers();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelUsersSave(FormCollection fc)
        {
            AdminMainProgress.PanelUsersSave(fc["Email"], fc["Password"], fc["Name"], fc["Surname"], int.Parse(fc["Role"]), int.Parse(Session["LoginId"].ToString()), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelUsers");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelUsersUpdate(FormCollection fc)
        {
            AdminMainProgress.PanelUsersUpdate(fc["Id"], fc["Email"], fc["Name"], fc["Surname"], int.Parse(fc["Role"]), int.Parse(Session["LoginId"].ToString()), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelUsers");
        }

        [HttpPost]
        public ActionResult PanelUsersIsActive(string Id, string Active)
        {
            var result = AdminMainProgress.PanelUsersIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //PanelUsers End

        //PanelMenu Start
        public ActionResult PanelMenu()
        {
            CreateSession();
            var result = AdminMainProgress.PanelMenuDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("PanelMenu");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPanelMenu = main.GetPanelMenu();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelMenuSave(FormCollection fc)
        {
            AdminMainProgress.PanelMenuSave(int.Parse(fc["ParentId"]), fc["Name"], fc["Url"], fc["Redirect"], int.Parse(fc["Access"]), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelMenu");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelMenuUpdate(FormCollection fc)
        {
            AdminMainProgress.PanelMenuUpdate(fc["Id"], int.Parse(fc["ParentId"]), fc["Name"], fc["Url"], fc["Redirect"], int.Parse(fc["Access"]), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelMenu");
        }

        [HttpPost]
        public ActionResult PanelMenuIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PanelMenuIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PanelMenuSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PanelMenuSort(ids, Session["Login"], db);
            return Json("success");
        }
        //PanelMenu End

        //Preferences Start
        public ActionResult Preferences()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Preferences = main.SiteGetPreferences();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PreferencesUpdate(FormCollection fc)
        {
            AdminMainProgress.PreferencesUpdate(fc["Logo"], fc["Favicon"], fc["GoogleAnalytics"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Preferences");
        }
        //Preferences End

        //Languages Start
        public ActionResult Languages()
        {
            CreateSession();
            var result = AdminMainProgress.LanguagesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Languages");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllLanguages = main.GetLanguages();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LanguagesSave(FormCollection fc)
        {
            AdminMainProgress.LanguagesSave(fc["Name"], fc["ShortName"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Languages");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LanguagesUpdate(FormCollection fc)
        {
            AdminMainProgress.LanguagesUpdate(fc["Id"], fc["Name"], fc["ShortName"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Languages");
        }

        [HttpPost]
        public ActionResult LanguagesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.LanguagesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult LanguagesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.LanguagesSort(ids, Session["Login"], db);
            return Json("success");
        }
        //Languages End

        //SocialMedia Start
        public ActionResult SocialMedia()
        {
            CreateSession();
            var result = AdminMainProgress.SocialMediaDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("SocialMedia");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllSocialMedia = main.GetSocialMedia();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SocialMediaSave(FormCollection fc)
        {
            AdminMainProgress.SocialMediaSave(fc["Name"], fc["Link"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("SocialMedia");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SocialMediaUpdate(FormCollection fc)
        {
            AdminMainProgress.SocialMediaUpdate(fc["Id"], fc["Name"], fc["Link"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("SocialMedia");
        }

        [HttpPost]
        public ActionResult SocialMediaIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.SocialMediaIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult SocialMediaSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.SocialMediaSort(ids, Session["Login"], db);
            return Json("success");
        }
        //SocialMedia End

        //MultiPreferences Start
        public ActionResult MultiPreferences()
        {
            CreateSession();
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllMultiPreferences = main.GetMultiPreferences();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MultiPreferencesUpdate(FormCollection fc)
        {
            AdminMainProgress.MultiPreferencesUpdate(int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Description"], fc["Keywords"], fc["Catalog"], fc["Footer"], fc["Address"], fc["SupportPhone"], fc["Phone"], fc["Fax"], fc["Email"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("MultiPreferences");
        }
        //MultiPreferences End

        //Pages Start
        public ActionResult Pages()
        {
            CreateSession();
            var result = AdminMainProgress.PagesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Pages");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPages = main.GetPages();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PagesSave(FormCollection fc)
        {
            AdminMainProgress.PagesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Banner"], fc["Thumbnail"], fc["Image"], fc["Video"], fc["File"], fc["View"], fc["ShowMenu"], fc["ShowFooter"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Pages");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PagesUpdate(FormCollection fc)
        {
            AdminMainProgress.PagesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Banner"], fc["Thumbnail"], fc["Image"], fc["Video"], fc["File"], fc["View"], fc["ShowMenu"], fc["ShowFooter"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Pages");
        }

        [HttpPost]
        public ActionResult PagesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PagesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PagesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PagesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Pages End

        //Translates Start
        public ActionResult Translates()
        {
            CreateSession();
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllTranslates = main.GetTranslates();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult TranslatesSave(FormCollection fc)
        {
            AdminMainProgress.TranslatesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Description"], fc["Text"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Translates");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult TranslatesUpdate(FormCollection fc)
        {
            AdminMainProgress.TranslatesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Description"], fc["Text"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Translates");
        }
        //Translates End

        //CampaignCategories Start
        public ActionResult CampaignCategories()
        {
            CreateSession();
            var result = AdminMainProgress.CampaignCategoriesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("CampaignCategories");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllCampaignCategories = main.GetCampaignCategories();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CampaignCategoriesSave(FormCollection fc)
        {
            AdminMainProgress.CampaignCategoriesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("CampaignCategories");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CampaignCategoriesUpdate(FormCollection fc)
        {
            AdminMainProgress.CampaignCategoriesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("CampaignCategories");
        }

        [HttpPost]
        public ActionResult CampaignCategoriesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.CampaignCategoriesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult CampaignCategoriesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.CampaignCategoriesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //CampaignCategories End

        //Campaigns Start
        public ActionResult Campaigns()
        {
            CreateSession();
            var result = AdminMainProgress.CampaignsDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Campaigns");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllCampaigns = main.GetCampaigns();
            main.AllCampaignCategories = main.GetCampaignCategories();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CampaignsSave(FormCollection fc)
        {
            AdminMainProgress.CampaignsSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["OldPrice"], fc["NewPrice"], fc["InfoText"], fc["InfoTextColor"], fc["Image"], Convert.ToDateTime(fc["StartDate"]), Convert.ToDateTime(fc["EndDate"]), fc["Hashtags"], fc["Favorite"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Campaigns");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CampaignsUpdate(FormCollection fc)
        {
            AdminMainProgress.CampaignsUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["OldPrice"], fc["NewPrice"], fc["InfoText"], fc["InfoTextColor"], fc["Image"], Convert.ToDateTime(fc["StartDate"]), Convert.ToDateTime(fc["EndDate"]), fc["Hashtags"], fc["Favorite"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Campaigns");
        }

        [HttpPost]
        public ActionResult CampaignsIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.CampaignsIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //Campaigns End

        //Slider Start
        public ActionResult Slider()
        {
            CreateSession();
            var result = AdminMainProgress.SliderDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Slider");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllSlider = main.GetSlider();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SliderSave(FormCollection fc)
        {
            AdminMainProgress.SliderSave(int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["Text"], fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Slider");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SliderUpdate(FormCollection fc)
        {
            AdminMainProgress.SliderUpdate(fc["Id"], int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["Text"], fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Slider");
        }

        [HttpPost]
        public ActionResult SliderIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.SliderIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult SliderSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.SliderSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Slider End

        //Channels Start
        public ActionResult Channels()
        {
            CreateSession();
            var result = AdminMainProgress.ChannelsDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Channels");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllChannels = main.GetChannels();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChannelsSave(FormCollection fc)
        {
            AdminMainProgress.ChannelsSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Text"], fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Channels");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChannelsUpdate(FormCollection fc)
        {
            AdminMainProgress.ChannelsUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Text"], fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Channels");
        }

        [HttpPost]
        public ActionResult ChannelsIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.ChannelsIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult ChannelsSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.ChannelsSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Channels End

        //Services Start
        public ActionResult Services()
        {
            CreateSession();
            var result = AdminMainProgress.ServicesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Services");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllServices = main.GetServices();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ServicesSave(FormCollection fc)
        {
            AdminMainProgress.ServicesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Content"], fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Services");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ServicesUpdate(FormCollection fc)
        {
            AdminMainProgress.ServicesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["Content"], fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Services");
        }

        [HttpPost]
        public ActionResult ServicesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.ServicesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //Services End

        //Dealerships Start
        public ActionResult Dealerships()
        {
            CreateSession();
            var result = AdminMainProgress.DealershipsDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Dealerships");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllDealerships = main.GetDealerships();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult DealershipsSave(FormCollection fc)
        {
            AdminMainProgress.DealershipsSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Image"], fc["Link"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Dealerships");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult DealershipsUpdate(FormCollection fc)
        {
            AdminMainProgress.DealershipsUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["Image"], fc["Link"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Dealerships");
        }

        [HttpPost]
        public ActionResult DealershipsIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.DealershipsIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //Dealerships End

        //ContactForms Start
        public ActionResult ContactForms()
        {
            CreateSession();
            var result = AdminMainProgress.ContactFormsDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("ContactForms");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllContactForms = main.GetContactForms();
            return View(main);
        }
        //ContactForms End

        //CustomerForms Start
        public ActionResult CustomerForms()
        {
            CreateSession();
            var result = AdminMainProgress.CustomerFormsDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("CustomerForms");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllCustomerForm = main.GetCustomerForms();
            return View(main);
        }
        //CustomerForms End
    }
}