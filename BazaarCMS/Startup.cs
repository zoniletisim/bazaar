﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BazaarCMS.Startup))]
namespace BazaarCMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
